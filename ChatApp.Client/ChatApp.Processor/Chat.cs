﻿using ChatApp.Client.Processor;
using ChatApp.Client.Processor.DataType;
using ClientProcessor;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Text.Json;

namespace ChatApp.Processor
{
    public delegate void LoginAcceptedDelegate(obj_user_login_accepted data, Chat chat);
    public delegate void UserConnectedDelegate(obj_user_connected data);
    public delegate void UserDisconnectedDelegate(obj_user_disconnected data, string name);
    public delegate void UserMessageDelegate(string name, obj_user_message data);
    public delegate void UserListDelegate(Dictionary<int, ChatUserBasic> data);
    public delegate void KickedDelegate(obj_kicked data);

    public class Chat : ClientProcessor.Client
    {
        public Dictionary<int, ChatUserBasic> _userList;
        private object lockChatUserList;
        public string name;
        public int userId;

        public event LoginAcceptedDelegate loginAcceptedEvent;
        public event UserConnectedDelegate userConnectedEvent;
        public event UserDisconnectedDelegate userDisconnectedEvent;
        public event UserMessageDelegate userMessageEvent;
        public event UserListDelegate userListEvent;
        public event ServerClosedDelegate serverClosedEvent;
        public event KickedDelegate kickedEvent;

        public Chat(string ip, int port) : base(ip, port)
        {

        }

        public bool Start(string name)
        {
            this.name = name;
            _userList = new Dictionary<int, ChatUserBasic>();
            lockChatUserList = new object();
            base.ServerClosedEvent += Client_ServerDisconnectedEvent;
            base.ServerConnectedEvent += Client_ServerConnectedEvent;
            base.ServerReceivedEvent += Client_ServerReceivedEvent;
            base.ServerClosedEvent += Client_ServerClosedEvent;
            return base.Connect();
        }

        private void Client_ServerClosedEvent()
        {
            serverClosedEvent?.Invoke();
        }

        public void Stop()
        {
            base.Close();
        }

        private void Client_ServerReceivedEvent(byte[] data)
        {
            // string jsonText = Encoding.UTF8.GetString(data);
            FrameType objType;
            try
            {
                objType = JsonSerializer.Deserialize<FrameType>(data);
            }
            catch { Trace.WriteLine("--> " + Encoding.UTF8.GetString(data)); return; }

            switch (objType.type)
            {
                case "user_login_accepted":
                    Trace.WriteLine("*");
                    user_login_accepted(objType.data);
                    break;
                case "user_connected":

                    user_connected(objType.data);
                    break;
                case "user_disconnected":
                    user_disconnected(objType.data);
                    break;
                case "user_message":
                    user_message(objType.data);
                    break;
                case "user_list":
                    user_list(objType.data);
                    break;
                case "kicked":
                    kicked(objType.data);
                    break;
                default:
                    Trace.WriteLine("--> " + Encoding.UTF8.GetString(data));
                    break;
            }
        }

        private void user_login_accepted(object objData)
        {
            obj_user_login_accepted data = JsonSerializer.Deserialize<obj_user_login_accepted>(objData.ToString());
            userId = data.userId;
            loginAcceptedEvent?.Invoke(data, this);
        }
        private void user_connected(object objData)
        {
            obj_user_connected data = JsonSerializer.Deserialize<obj_user_connected>(objData.ToString());
            AddChatUser(new ChatUserBasic { name = data.name, userId = data.id });
            userConnectedEvent?.Invoke(data);
        }
        private void user_disconnected(object objData)
        {
            obj_user_disconnected data = JsonSerializer.Deserialize<obj_user_disconnected>(objData.ToString());
            try
            {
                userDisconnectedEvent?.Invoke(data, _userList[data.id].name);
                RemoveChatUser(data.id);
                Trace.WriteLine(data.id);
            }
            catch { }

        }
        private void user_message(object objData)
        {
            obj_user_message data = JsonSerializer.Deserialize<obj_user_message>(objData.ToString());
            if (data.userId != 0)
                userMessageEvent?.Invoke(_userList[data.userId].name, data);
            else
                userMessageEvent?.Invoke("", data);
        }
        private void user_list(object objData)
        {
            Dictionary<int, ChatUserBasic> data = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<int, ChatUserBasic>>(objData.ToString());
            SetChatUser(data);
            userListEvent?.Invoke(data);
            //   Trace.WriteLine("User Count: " + data.Count);
        }
        private void kicked(object objData)
        {
            obj_kicked data = Newtonsoft.Json.JsonConvert.DeserializeObject<obj_kicked>(objData.ToString());
            kickedEvent?.Invoke(data);
        }
        public void SendMessage(string msg)
        {
            FrameType objType = new FrameType();
            objType.type = "user_message";
            obj_user_message obj_User_Message = new obj_user_message { message = msg };
            objType.data = obj_User_Message;

            Send(objType);
        }
        private void Send(object obj)
        {
            base.Send(JsonSerializer.Serialize(obj));
        }

        public void GetUserList()
        {
            FrameType objType = new FrameType();
            objType.type = "user_list";
            Send(objType);
        }

        private void Client_ServerConnectedEvent()
        {
            obj_login obj_Login = new obj_login { name = this.name };
            FrameType objType = new FrameType { type = "login", data = obj_Login };

            Send(objType);
        }

        private void Client_ServerDisconnectedEvent()
        {

        }

        private void SetChatUser(Dictionary<int, ChatUserBasic> data)
        {
            lock (lockChatUserList)
            {
                _userList = data;
            }
        }

        private void AddChatUser(ChatUserBasic user)
        {
            lock (lockChatUserList)
            {
                _userList.Add(user.userId, user);
            }
        }

        private void RemoveChatUser(int userId)
        {
            lock (lockChatUserList)
            {
                _userList.Remove(userId);
            }
        }
    }
}
