﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatApp.Client.Processor.DataType
{
    public class obj_user_login_accepted
    {
        public int userId { set; get; }
        public string name { set; get; }
    }
}
