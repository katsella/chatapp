﻿using ChatApp.Client.Processor;
using ChatApp.Client.Processor.DataType;
using ChatApp.Processor;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ChatApp.Client
{
    /// <summary>
    /// Interaction logic for ChatWindow.xaml
    /// </summary>
    public partial class ChatWindow : Window
    {
        Chat chat;
        private object lockUserLBox;
        private bool AutoScroll = true;
        private int lastMessageUserId;

        public ChatWindow(Chat chat)
        {
            InitializeComponent();
            this.chat = chat;
            chat.userConnectedEvent += Chat_userConnectedEvent;
            chat.userDisconnectedEvent += Chat_userDisconnectedEvent;
            chat.userListEvent += Chat_userListEvent;
            chat.userMessageEvent += Chat_userMessageEvent;
            chat.serverClosedEvent += Chat_serverClosedEvent;
            chat.kickedEvent += Chat_kickedEvent;
            lockUserLBox = new object();
            lastMessageUserId = -1;

            this.Title = chat.name + "(" + chat.userId + ")";
            AddAdminMessage("Chat'e katıldınız.");
        }

        private void Chat_kickedEvent(obj_kicked data)
        {
            if (!String.IsNullOrEmpty(data.kick_message))
                MessageBox.Show(data.kick_message, "Atıldın", MessageBoxButton.OK, MessageBoxImage.Error);
            Environment.Exit(0);
        }

        private void Chat_serverClosedEvent()
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                this.Close();
            });
        }

        private void btnSend_Click(object sender, RoutedEventArgs e)
        {
            MessageSend();
        }

        private void txtMyMessage_KeyUp(object sender, KeyEventArgs e)
        {
            TextBox txt = (TextBox)sender;
            if (txt.Text.Length > 0 && !Char.IsUpper(txt.Text[0]))
            {
                txt.Text = txt.Text.Substring(0, 1).ToUpper() + txt.Text.Substring(1);
                txt.SelectionStart = txt.Text.Length;
                txt.SelectionLength = 0;
            }

            if (e.Key == Key.Enter)
            {
                MessageSend();
            }
        }

        private void MessageSend()
        {
            if (txtMessage.Text.Length == 0)
                return;
            AddRightMessage(txtMessage.Text);
            chat.SendMessage(txtMessage.Text);
            Application.Current.Dispatcher.Invoke(() =>
            {
                txtMessage.Text = "";
            });
        }

        private void Chat_userMessageEvent(string name, obj_user_message data)
        {
            switch (data.userType)
            {
                case 0:
                    AddLeftMessage(name, data);
                    break;
                case 1:
                    AddAdminMessage(data.message);
                    break;
            }



        }

        private void AddLeftMessage(string name, obj_user_message data)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                if (lastMessageUserId != data.userId)
                {
                    Grid grid = new Grid { HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(0, 5, 25, 0) };
                    Border border = new Border { Padding = new Thickness(3, 3, 3, 3), Background = Brushes.White, BorderBrush = (Brush)new BrushConverter().ConvertFrom("#E0E0E0"), BorderThickness = new Thickness(1, 1, 1, 1), CornerRadius = new CornerRadius(7, 7, 7, 7) };
                    Grid gridIn = new Grid();
                    Label lblName = new Label { Content = name, HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(0, 0, 0, 0), FontSize = 13, Foreground = Brushes.Blue, FontWeight = FontWeights.Bold, FontFamily = new FontFamily("Arial Rounded MT Bold"), VerticalAlignment = VerticalAlignment.Top, FontStyle = FontStyles.Normal };
                    TextBlock lblMsg = new TextBlock { Text = data.message, TextWrapping = TextWrapping.Wrap, HorizontalAlignment = HorizontalAlignment.Left, VerticalAlignment = VerticalAlignment.Top, Margin = new Thickness(5, 25, 6, 6), FontSize = 14, FontWeight = FontWeights.SemiBold, FontFamily = new FontFamily("Arial Rounded MT Bold"), FontStyle = FontStyles.Normal };
                    gridIn.Children.Add(lblName);
                    gridIn.Children.Add(lblMsg);
                    border.Child = gridIn;
                    grid.Children.Add(border);
                    messageStack.Children.Add(grid);
                    lastMessageUserId = data.userId;
                }
                else
                {
                    Application.Current.Dispatcher.Invoke(() =>
                    {
                        Grid grid = new Grid { HorizontalAlignment = HorizontalAlignment.Left, Margin = new Thickness(0, 2, 25, 0) };
                        Border border = new Border { Padding = new Thickness(3, 3, 3, 3), Background = Brushes.White, BorderBrush = (Brush)new BrushConverter().ConvertFrom("#E0E0E0"), BorderThickness = new Thickness(1, 1, 1, 1), CornerRadius = new CornerRadius(7, 7, 7, 7) };
                        TextBlock label = new TextBlock { Text = data.message, TextWrapping = TextWrapping.Wrap, Margin = new Thickness(6, 4, 6, 4), HorizontalAlignment = HorizontalAlignment.Left, FontSize = 14, FontWeight = FontWeights.SemiBold, FontStyle = FontStyles.Normal, FontFamily = new FontFamily("Arial Rounded MT Bold") };
                        border.Child = label;
                        grid.Children.Add(border);

                        messageStack.Children.Add(grid);
                    });
                }



            });
        }

        private void AddRightMessage(string msg)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                lastMessageUserId = -1;
                Grid grid = new Grid { HorizontalAlignment = HorizontalAlignment.Right, Margin = new Thickness(25, 2, 0, 0) };
                Border border = new Border { Padding = new Thickness(3, 3, 3, 3), Background = (Brush)new BrushConverter().ConvertFrom("#DCF8C6"), BorderBrush = (Brush)new BrushConverter().ConvertFrom("#E0E0E0"), BorderThickness = new Thickness(1, 1, 1, 1), CornerRadius = new CornerRadius(7, 7, 7, 7) };
                TextBlock label = new TextBlock { Text = msg, TextWrapping = TextWrapping.Wrap, Margin = new Thickness(6, 4, 6, 4), HorizontalAlignment = HorizontalAlignment.Right, FontSize = 14, FontWeight = FontWeights.SemiBold, FontStyle = FontStyles.Normal, FontFamily = new FontFamily("Arial Rounded MT Bold") };
                border.Child = label;
                grid.Children.Add(border);

                messageStack.Children.Add(grid);
            });
        }

        private void AddAdminMessage(string msg)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                lastMessageUserId = 0;
                Grid grid = new Grid { HorizontalAlignment = HorizontalAlignment.Center };
                Border border = new Border { Padding = new Thickness(3, 1, 1, 3), Background = (Brush)new BrushConverter().ConvertFrom("#FFF4C4"), BorderBrush = (Brush)new BrushConverter().ConvertFrom("#E0E0E0"), BorderThickness = new Thickness(1, 1, 1, 1), CornerRadius = new CornerRadius(5, 5, 5, 5) };
                TextBlock label = new TextBlock { Text = msg, TextWrapping = TextWrapping.Wrap, Margin = new Thickness(6, 6, 6, 6), HorizontalAlignment = HorizontalAlignment.Center, FontSize = 11, FontWeight = FontWeights.Bold, FontStyle = FontStyles.Normal, FontFamily = new FontFamily("Arial Rounded MT Bold") };
                border.Child = label;
                grid.Children.Add(border);

                messageStack.Children.Add(grid);
            });
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            chat.GetUserList();
        }

        private void Chat_userDisconnectedEvent(Processor.DataType.obj_user_disconnected data, string name)
        {

            Application.Current.Dispatcher.Invoke(() =>
            {
                RemoveUserToListBox(name + "(" + data.id.ToString() + ")");
            });
        }

        private void Chat_userListEvent(Dictionary<int, ChatUserBasic> data)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                foreach (var item in data)
                {
                    AddUserToListBox(item.Value.name + "(" + item.Value.userId.ToString() + ")");
                }
            });
        }

        private void Chat_userConnectedEvent(Processor.DataType.obj_user_connected data)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                AddUserToListBox(data.name + "(" + data.id.ToString() + ")");
            });
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            chat.Stop();
        }

        private void AddUserToListBox(string msg)
        {
            lock (lockUserLBox)
            {
                lBoxUser.Items.Add(msg);
            }
        }
        private void RemoveUserToListBox(string msg)
        {
            lock (lockUserLBox)
            {
                lBoxUser.Items.Remove(msg);
            }
        }

        private void messageStackScroller_ScrollChanged(object sender, ScrollChangedEventArgs e)
        {
            if (e.ExtentHeightChange == 0)
            {
                if (messageStackScroller.VerticalOffset == messageStackScroller.ScrollableHeight)
                {
                    AutoScroll = true;
                }
                else
                {
                    AutoScroll = false;
                }
            }
            if (AutoScroll && e.ExtentHeightChange != 0)
            {
                messageStackScroller.ScrollToVerticalOffset(messageStackScroller.ExtentHeight);
            }
        }
    }
}
