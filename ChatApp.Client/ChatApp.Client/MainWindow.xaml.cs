﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ChatApp.Processor;
using ClientProcessor;

namespace ChatApp.Client
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }
        
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
         //   for (int i = 0; i < 1000; i++)
            {
                Chat chat;
                string[] parts = txtIpAdress.Text.Split(':');
                int port = 9876;
                if (parts.Length == 2)
                {
                    try { port = int.Parse(parts[1]); } catch { }
                }

                chat = new Chat(parts[0], port);
              //    if(i%10==0)
                chat.loginAcceptedEvent += Chat_loginAcceptedEvent;
                chat.Start(txtName.Text);
            }
        }
        protected override void OnClosing(System.ComponentModel.CancelEventArgs e)
        {
            Environment.Exit(0);
            base.OnClosing(e);
        }

        private void Chat_loginAcceptedEvent(Processor.DataType.obj_user_login_accepted data, Chat chat)
        {
            Application.Current.Dispatcher.Invoke(() =>
            {
                ChatWindow chatWindow = new ChatWindow(chat);
                chatWindow.Show();
                this.Hide();
            });
        }
    }
}
