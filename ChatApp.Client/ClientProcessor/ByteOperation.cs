﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ClientProcessor
{
    public class ByteOperation
    {
        public static int PaternSearchFirst(ref byte[] source, int sourceSize, int offset, byte[] patern)
        {
            int i = offset, k = 0;
            while (i < sourceSize)
            {
                while (source[i] == patern[k])
                {
                    k++;
                    i++;
                    if (patern.Length == k)
                        return i - k;
                }
                k = 0;
                i++;
            }
            return -1;
        }
    }
}
