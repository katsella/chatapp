﻿using System;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace ClientProcessor
{
    public delegate void ServerConnectedDelegate();
    public delegate void ServerClosedDelegate();
    public delegate void ServerReceivedDelegate(byte[] data);
    public class Client
    {
        private readonly string ip;
        private readonly int port;
        private bool isConnected;
        private readonly byte[] separator = new byte[] { 250, 250, 250 };

        public event ServerReceivedDelegate ServerReceivedEvent;
        public event ServerConnectedDelegate ServerConnectedEvent;
        public event ServerClosedDelegate ServerClosedEvent;

        private Server server;

        public Client(string ip, int port)
        {
            this.ip = ip;
            this.port = port;
            isConnected = false;
        }

        public bool Connect()
        {

            try
            {
                server = new Server(new TcpClient(ip, port));
                isConnected = true;
                ServerConnectedEvent?.Invoke();
                server.tcpClient.Client.BeginReceive(server.buffer, 0, server.buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), server);
            }
            catch
            {
                isConnected = false;
                return false;
            }
            return true;
        }
        public void Close()
        {
            try
            {
                server.tcpClient.Client.Close();
            }
            catch { }
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            Server server = (Server)ar.AsyncState;
            int size = 0;
            try
            {
                size = server.tcpClient.Client.EndReceive(ar);
            }
            catch { Disconnect(); return; }

            int index = 0;
            int temp = 0;
            while (index + separator.Length < size)
            {
                temp = ByteOperation.PaternSearchFirst(ref server.buffer, size, index, separator);

                if(temp != -1)
                {
                    byte[] cleanData = new byte[temp-index];
                    Array.Copy(server.buffer, index ,cleanData, 0, temp - index);
                    Task.Run(() =>
                    {
                        ServerReceivedEvent?.Invoke(cleanData);
                    });
                }
                else
                {
                    break;
                }

                index = temp + separator.Length;
            }

            try
            {
                server.tcpClient.Client.BeginReceive(server.buffer, 0, server.buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), server);
            }
            catch { Disconnect(); return; }
        }

        private void Disconnect()
        {
            isConnected = false;
            ServerClosedEvent?.Invoke();
        }

        public bool Send(byte[] data)
        {
            if (!isConnected)
                return false;
            byte[] newArray = new byte[data.Length + separator.Length];
            Array.Copy(data, newArray, data.Length);
            Array.Copy(separator, 0, newArray, data.Length, separator.Length);

            try
            {
                server.tcpClient.Client.Send(newArray);
            }
            catch
            {
                return false;
            }
            return true;
        }

        public bool Send(string msg)
        {
            return Send(Encoding.UTF8.GetBytes(msg));
        }
    }
}
