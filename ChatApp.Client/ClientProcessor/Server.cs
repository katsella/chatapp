﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;

namespace ClientProcessor
{
    public class Server
    {
        internal TcpClient tcpClient;
        internal byte[] buffer;

        public Server(TcpClient tcpClient)
        {
            this.tcpClient = tcpClient;
            buffer = new byte[81920];
        }
    }
}
