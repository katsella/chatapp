﻿using System;
using System.Collections.Generic;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;

namespace ServerProcessor
{
    public class User : IDisposable
    {
        bool disposed = false;
        internal TcpClient tcpClient;
        public int userId;
        internal byte[] buffer;

        public User(TcpClient tcpClient, int userId)
        {
            this.tcpClient = tcpClient;
            this.userId = userId;
            this.buffer = new byte[8192];
        }

        public void Send(byte[] data)
        {
            if (disposed)
                return;
            byte[] newArray = new byte[data.Length + Server.separator.Length];
            Array.Copy(data, newArray, data.Length);
            Array.Copy(Server.separator, 0, newArray, data.Length, Server.separator.Length);

            tcpClient.Client.SendAsync(newArray, SocketFlags.None);
        }

        public void Send(object obj)
        {
            if (disposed)
                return;
            Send(Encoding.UTF8.GetBytes(JsonSerializer.Serialize(obj)));
        }

        internal void Close()
        {
            try
            {
                tcpClient.Client.Close();
            }
            catch { }
        }

        public void Dispose()
        {
            tcpClient.Dispose();
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                tcpClient.Dispose();
                buffer = null;
            }
            disposed = true;
        }

    }
}
