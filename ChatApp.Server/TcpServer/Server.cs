﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace ServerProcessor
{
    public delegate void UserConnectedDelegate(User user);
    public delegate void UserDisconnecdetDelegate(User user);
    public delegate void UserReceivedDelegate(User user, byte[] data);
    public class Server : IDisposable
    {
        private int port;
        private bool isRunning;
        private int userCounter;
        public static readonly byte[] separator = new byte[] { 250, 250, 250 };
        private bool disposed = false;

        private Dictionary<int, User> userList;
        private object lockUserList;
        private int userCountPeak;


        private TcpListener tcpServer;

        public event UserConnectedDelegate userConnectedEvent;
        public event UserDisconnecdetDelegate userDisconnectedEvent;
        public event UserReceivedDelegate userReceivedEvent;

        public Server(int port)
        {
            this.port = port;
            isRunning = false;
            userCounter = 1;
            userCountPeak = 1;
            tcpServer = new TcpListener(IPAddress.Any, port);
            userList = new Dictionary<int, User>();
            lockUserList = new object();
        }

        public bool Start()
        {
            try
            {
                if (isRunning)
                    return true;
                tcpServer.Start();
                tcpServer.BeginAcceptTcpClient(new AsyncCallback(AcceptCallback), null);
            }
            catch { return false; }
            isRunning = true;
            return true;
        }

        private void AcceptCallback(IAsyncResult ar)
        {
            User user;
            try
            {
                try
                {
                    if (tcpServer != null)
                        user = new User(tcpServer.EndAcceptTcpClient(ar), userCounter);
                    else
                        return;
                }
                catch (ObjectDisposedException e) { return; }
            }
            catch (NullReferenceException ex) { return; }
            userCounter++;
            AddUser(user);
            user.tcpClient.Client.BeginReceive(user.buffer, 0, user.buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), user);

            tcpServer.BeginAcceptTcpClient(new AsyncCallback(AcceptCallback), null);
        }

        private void ReceiveCallback(IAsyncResult ar)
        {
            User user = (User)ar.AsyncState;
            int size = 0;

            try
            {
                size = user.tcpClient.Client.EndReceive(ar);
                if (size == 0)
                {
                    UserDisconnected(ref user); return;
                }
            }
            catch { UserDisconnected(ref user); return; }

            int index = 0;
            int temp = 0;
            while (index + separator.Length < size)
            {
                temp = ByteOperation.PaternSearchFirst(ref user.buffer, size, index, separator);

                if (temp != -1)
                {
                    byte[] cleanData = new byte[temp - index];
                    Array.Copy(user.buffer, index, cleanData, 0, temp - index);
                    Task.Run(() =>
                    {
                        userReceivedEvent?.Invoke(user, cleanData);
                    });
                }

                index = temp;
            }

            try
            {
                user.tcpClient.Client.BeginReceive(user.buffer, 0, user.buffer.Length, SocketFlags.None, new AsyncCallback(ReceiveCallback), user);
            }
            catch { UserDisconnected(ref user); return; }
        }

        private void UserDisconnected(ref User user)
        {
            RemoveUser(ref user);
        }

        public void SendAll(byte[] data)
        {
            lock (lockUserList)
            {
                foreach (var item in userList)
                {
                    try
                    {
                        item.Value.Send(data);
                    }
                    catch { }
                }
            }
        }
        public void SendAll(object obj)
        {
            SendAll(Encoding.UTF8.GetBytes(JsonSerializer.Serialize(obj)));
        }

        public void SendAll(byte[] data, int senderId)
        {
            User item = new User(null, -1); ;
            for (int i = userList.Count - 1; i >= 0; i--)
            {
                try
                {
                    item = userList.ElementAt(i).Value;
                    if (item.userId != senderId)
                        item.Send(data);
                }
                catch (Exception e) { Console.WriteLine("--> Server-SendAll --> " + item.userId.ToString() + " --> " + e.Message); }

            }
        }
        public void SendAll(object obj, int senderId)
        {
            SendAll(Encoding.UTF8.GetBytes(JsonSerializer.Serialize(obj)), senderId);
        }

        private void AddUser(User user)
        {
            lock (lockUserList)
            {
                if (userCountPeak < userList.Count)
                    userCountPeak = userList.Count;

                userList.Add(user.userId, user);
                userConnectedEvent?.Invoke(user);
            }
        }
        private void RemoveUser(ref User user)
        {
            lock (lockUserList)
            {
                userDisconnectedEvent?.Invoke(user);
                user.Dispose();
                userList.Remove(user.userId);

                userList.TrimExcess();
            }
        }

        public void RemoveUser(int userId)
        {
            try
            {
                User user = userList[userId];
                user.tcpClient.Client.Dispose();
            }
            catch { }

        }

        public void Stop()
        {
            tcpServer.Stop();
            isRunning = false;
            lock (lockUserList)
            {
                foreach (var item in userList)
                {
                    item.Value.tcpClient.Client.Close();
                }
                userList.Clear();
            }

        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed) return;

            if (disposing)
            {
                Stop();
                tcpServer = null;
            }
            GC.Collect();
            disposed = true;
        }
    }
}
