﻿using ChatApp.Server.Processor.DataType;
using ServerProcessor;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json;

namespace ChatApp.Server.Processor
{
    public class Chat : ServerProcessor.Server
    {
        private Dictionary<int, ChatUser> _userList;
        private Dictionary<int, ChatUserBasic> _userListBasic;
        private object lockChatUserList;
        private bool disposed = false;
        private User serverUser;
        private bool consoloInfo = true;

        public Chat(int port) : base(port)
        {

        }

        public new void Start()
        {
            _userList = new Dictionary<int, ChatUser>();
            _userListBasic = new Dictionary<int, ChatUserBasic>();
            serverUser = new User(null, 0);
            lockChatUserList = new object();
            base.userConnectedEvent += TcpServer_userConnectedEvent;
            base.userReceivedEvent += TcpServer_userReceivedEvent;
            base.userDisconnectedEvent += TcpServer_userDisconnectedEvent;
            base.Start();
        }

        private void ConsoleWrite(string text)
        {
            if (consoloInfo)
            {
                Console.WriteLine("\r" + text);
                Console.Write("# ");
            }
        }

        public bool ServerCommand(string command)
        {
            if (String.IsNullOrEmpty(command))
                return true;

            if (command.StartsWith("\\") || command.StartsWith("/"))
            {
                command = command.Replace("  ", " ").ToLower();
                command = command.Remove(0, 1);
                try
                {
                    string[] splits = command.Split(' ', 2);
                    int temp;
                    switch (splits[0])
                    {
                        case "clear":
                            Console.Clear();
                            return true;
                        case "start":
                            base.Start();
                            Console.WriteLine("Running...");
                            return true;
                        case "stop":
                            base.Stop();
                            Console.WriteLine("Stopped.");
                            return true;
                        case "close":
                            Environment.Exit(0);
                            return true;
                        case "kick":
                            try
                            {
                                splits = splits[1].Split(' ', 2);
                                temp = int.Parse(splits[0]); // userid 
                                string message = "";
                                if (splits.Length >= 2)
                                    message = splits[1];
                                user_kicked(_userList[temp].user, message);
                                base.RemoveUser(temp);
                            }
                            catch { return false; };
                            return true;
                        case "noinfo":
                            consoloInfo = false;
                            return true;
                        case "info":
                            consoloInfo = true;
                            return true;
                        case "?":
                            Console.WriteLine(@"--------------------
start
stop
clear
noinfo
info
kick {userId} {message}
--------------------
                            ");
                            return true;

                    }
                }
                catch { }
            }
            else
            {

                FrameType objType = new FrameType();
                objType.type = "user_message";
                objType.data = new obj_user_message { userId = 0, message = command, userType = 1 };

                base.SendAll(objType);
                return true;
            }
            return false;
        }

        private void TcpServer_userReceivedEvent(User user, byte[] data)
        {

            FrameType objType;
            try
            {
                objType = JsonSerializer.Deserialize<FrameType>(data);
            }
            catch { ConsoleWrite("--> " + Encoding.UTF8.GetString(data)); return; }



            switch (objType.type)
            {
                case "login":
                    login(user, objType.data);
                    break;
                case "user_message":
                    user_message(user, objType.data);
                    break;
                case "user_list":
                    user_list(user);
                    break;
                default:
                    ConsoleWrite("--> " + Encoding.UTF8.GetString(data));
                    break;
            }
        }

        private void login(User user, object objData)
        {
            obj_login data = JsonSerializer.Deserialize<obj_login>(objData.ToString());

            FrameType objType = new FrameType();
            //-------------- User Accepted--------------
            objType.type = "user_login_accepted";

            obj_user_login_accepted obj_User_Login_Accepted = new obj_user_login_accepted { name = data.name, userId = user.userId };
            objType.data = obj_User_Login_Accepted;
            user.Send(objType);
            //------------------------------------------


            AddChatUser(user, data.name);
            ConsoleWrite(" User(" + user.userId + ") --> " + data.name);


            objType.type = "user_connected";
            obj_user_connected obj_User_Connected = new obj_user_connected { name = data.name, id = user.userId };
            objType.data = obj_User_Connected;

            base.SendAll(objType, user.userId);
        }

        private void user_list(User user)
        {
            FrameType objType = new FrameType();
            objType.type = "user_list";
            objType.data = Newtonsoft.Json.JsonConvert.SerializeObject(_userListBasic, Newtonsoft.Json.Formatting.Indented);
            user.Send(objType);
        }

        private void user_message(User user, object objData)
        {
            obj_user_message data = JsonSerializer.Deserialize<obj_user_message>(objData.ToString());
            data.userId = user.userId;
            data.userType = 0;

            FrameType objType = new FrameType();
            objType.type = "user_message";
            objType.data = data;

            base.SendAll(objType, user.userId);
        }

        private void user_kicked(User user, string message)
        {
            FrameType objType = new FrameType();
            objType.type = "kicked";
            obj_kicked obj_Kicked = new obj_kicked { kick_message = message };
            objType.data = Newtonsoft.Json.JsonConvert.SerializeObject(obj_Kicked, Newtonsoft.Json.Formatting.Indented);

            user.Send(objType);
        }

        private void TcpServer_userConnectedEvent(User user)
        {
            ConsoleWrite(" User(" + user.userId + ") --> Connected");
        }

        private void TcpServer_userDisconnectedEvent(User user)
        {
            ConsoleWrite(" User(" + user.userId + ") --> Disconnected");

            RemoveChatUser(user.userId);

            FrameType objType = new FrameType();
            objType.type = "user_disconnected";
            obj_user_disconnected obj_User_Disconnected = new obj_user_disconnected { id = user.userId };
            objType.data = obj_User_Disconnected;

            base.SendAll(objType);

        }

        private void AddChatUser(User user, string name)
        {
            lock (lockChatUserList)
            {
                _userList.Add(user.userId, new ChatUser(user, name));
                _userListBasic.Add(user.userId, new ChatUserBasic { userId = user.userId, name = name });
            }

        }

        private void RemoveChatUser(int userId)
        {
            lock (lockChatUserList)
            {
                _userList.Remove(userId);
                _userListBasic.Remove(userId);
            }
        }
        public void Dispose()
        {
            if (disposed) return;


            base.userConnectedEvent -= TcpServer_userConnectedEvent;
            base.userReceivedEvent -= TcpServer_userReceivedEvent;
            base.userDisconnectedEvent -= TcpServer_userDisconnectedEvent;

            _userList.Clear();
            _userListBasic.Clear();

            base.Dispose();

            GC.Collect();
            disposed = true;
        }
    }
}
