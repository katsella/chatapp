﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatApp.Server.Processor.DataType
{
    public class obj_user_connected
    {
        public string name { set; get; }
        public int id { set; get; }
    }
}
