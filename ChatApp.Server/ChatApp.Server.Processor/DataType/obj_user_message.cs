﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatApp.Server.Processor.DataType
{
    public class obj_user_message
    {
        public int userId { set; get; }
        public string message { set; get; }
        public int userType { set; get; }
    }
}
