﻿using ServerProcessor;
using System;
using System.Collections.Generic;
using System.Text;

namespace ChatApp.Server.Processor
{
    public class ChatUser
    {
        public User user;
        public string name;
        public ChatUser(User user, string name)
        {
            this.user = user;
            this.name = name;
        }
    }
}
