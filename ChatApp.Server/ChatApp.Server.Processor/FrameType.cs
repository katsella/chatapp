﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ChatApp.Server.Processor
{
    public class FrameType
    {
        public string type { set; get; }
        public object data { set; get; }
    }
}
